﻿using System;
namespace scheduler.interfaces.dal.model
{
    public class AppointmentAttendee
    {
        protected AppointmentAttendee() {

        }

        public AppointmentAttendee(long id, OneOffMeeting appointment, long appointmentId, Attendee attendee, long attendeId)
        {
            Id = id;
            Appointment = appointment;
            AppointmentId = appointmentId;
            Attendee = attendee;
            AttendeeId = attendeId;
        }

        public long Id { get; set; }
        public OneOffMeeting Appointment { get; set; }
        public long AppointmentId { get; set; }
        public Attendee Attendee { get; set; }
        public long AttendeeId { get; set; }

        public scheduler.interfaces.bll.model.Attendee ToModel() {
            if (this.Attendee == null)
                throw new ArgumentNullException(nameof(this.Attendee), "Can not convert AppointmentAttendee to Attendee if appointmentAttendee.Attendee is null!");
                
            return new scheduler.interfaces.bll.model.Attendee(this.Attendee.Id);
        }
    }
}
