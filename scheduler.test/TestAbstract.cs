using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Xunit;

namespace scheduler.test
{
    public abstract class TestAbstract
    {
        protected ILoggerFactory _loggerFactory;
        protected ILogger _logger;

        public TestAbstract() {
            var builder = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json");

            var cnf = builder.Build();
            _loggerFactory = new Microsoft.Extensions.Logging.LoggerFactory();

            _loggerFactory.AddConsole(cnf.GetSection("Logging"));
            _logger = _loggerFactory.CreateLogger<TestAbstract>();

        }
    }
}
