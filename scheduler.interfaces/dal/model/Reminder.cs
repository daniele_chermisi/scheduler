﻿using System;
using scheduler.interfaces.bll.model;

namespace scheduler.interfaces.dal.model
{
    public class Reminder : Appointment
    {
        public Reminder(long id, DateTime start) : base(id, start)
        {
        
        }

        public override DateTime GetMinDate => Start;

        public override DateTime GetMaxDate => Start;

        public override bool IsConflicting(IMeetingOccurrence other, IOccurrenceComparator comp)
        {
            return false;
        }

        public override bll.model.Appointment ToModel()
        {
            return new bll.model.Reminder(this.Id, this.Start);
        }

        public override void UpdateDate(DateTime newDateTime)
        {
            this.Start = newDateTime;
        }
    }
}
