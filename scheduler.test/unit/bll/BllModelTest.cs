﻿using System;
using Xunit;
using scheduler.bll;
using System.Collections.Generic;
using scheduler.test.dal;
using System.Linq;
using scheduler.interfaces.bll.model;

namespace scheduler.test.unit.bll
{
    [Trait("Category", "Unit")]
    public class BllModelTest
    {
        private readonly ObjectMother _mother;

        public BllModelTest()
        {
            _mother = new ObjectMother();
        }

        [Fact]
        public void ToModel_Collection_RetunsCorrectInstancesType() {
            var absList = new List<scheduler.interfaces.dal.model.Appointment>()
            {
                _mother.Persisted.Reminder,
                _mother.Persisted.OneOffMeeting,
                _mother.Persisted.RecurrentMeeting,
            };

            var modelList = absList.Select(m => m.ToModel()).ToList();
            var item = modelList.Single(m => m.Id == _mother.Persisted.Reminder.Id);
            Assert.IsType<Reminder>(item);   
            item = modelList.Single(m => m.Id == _mother.Persisted.OneOffMeeting.Id);
            Assert.IsType<OneOffMeeting>(item);   
            item = modelList.Single(m => m.Id == _mother.Persisted.RecurrentMeeting.Id);
            Assert.IsType<RecurrentMeeting>(item);   
        }
    }
}
