using System;
namespace scheduler.interfaces.dal.model
{
    public abstract class Appointment
    {
        public Appointment(long id, DateTime start)
        {
            Id = id;
            Start = start;
        }

        public long Id { get; protected set; }
        public DateTime Start { get; set; }

        public abstract DateTime GetMinDate { get; }
        public abstract DateTime GetMaxDate { get; }

        public abstract bool IsConflicting(IMeetingOccurrence other, IOccurrenceComparator comp);
        public abstract void UpdateDate(DateTime newDateTime);
        public abstract scheduler.interfaces.bll.model.Appointment ToModel();

    }
}