﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using scheduler.interfaces.dal.model;
using Xunit;

namespace scheduler.test.dal
{
    // https://docs.microsoft.com/it-it/ef/core/modeling/relational/inheritance
    [Trait("Category", "Integration")]
    public class EFMappingTest : NeverCommitIntegrationTest
    {
        private readonly ObjectMother _mother;

        public EFMappingTest()
        {
            this._mother = new ObjectMother();
        }

        [Fact]
        public void GetAppointments_ShouldHaveBothMeetendAndReminder()
        {
            // ACT
            var all = _db.Appointments.ToList();
            var reminder = all.FirstOrDefault(a => a.Id == 1);
            var oneOffMeeting = all.FirstOrDefault(a => a.Id == 2);

            // ASSERT
            Assert.Equal(typeof(Reminder), reminder.GetType());
            Assert.IsType<Reminder>(reminder);
            Assert.Equal(typeof(OneOffMeeting), oneOffMeeting.GetType());
            Assert.IsType<OneOffMeeting>(oneOffMeeting);
        }

        [Fact]
        public void GetAppointments_RecurringMeeting_ShouldHaveRepetitions()
        {
            // ACT
            var recMeeting = _db.RecurrentMeetings
                .Include(a => a.Repetitions)
                .Include(a => a.Attenders).ThenInclude(att => att.Attendee)
                .Single(a => a.Id == _mother.Persisted.RecurrentMeeting.Id);

            // ASSERT
            Assert.IsType<RecurrentMeeting>(recMeeting);
            RecurrentMeeting recMeetingCasted = recMeeting as RecurrentMeeting;
            var occorrences = recMeetingCasted.Repetitions;
            Assert.NotEmpty(occorrences);
            Assert.Equal(new DateTime(2018, 02, 8, 8, 30, 0), occorrences.First().Start);
            Assert.NotEmpty(recMeetingCasted.Attenders.Select(a => a.Attendee));
        }
    }
}
