﻿using System;
using System.Collections.Generic;

namespace scheduler.web.dto.model
{
    public class UpdateAttendeesListRequest
    {
        public IList<long> Attendees { get; set; }
        public UpdateAttendeesListRequest()
        {
        }

        public override string ToString() 
        {
            return $"Attendees:{Attendees}";
        }
    }
}
