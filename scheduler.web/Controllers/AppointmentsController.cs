﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using scheduler.Data;
using scheduler.interfaces.bll;
using scheduler.interfaces.bll.model;
using scheduler.interfaces.dal;
using scheduler.web.dto.model;

namespace scheduler.web.Controllers
{
    [Route("api/[controller]")]
    public class AppointmentsController : Controller
    {
        private readonly IAppointmentService _service;
        private readonly ILogger _logger;

        public AppointmentsController(IAppointmentService service, ILoggerFactory lf)
        {
            this._service = service;
            _logger = lf.CreateLogger<AppointmentsController>();
        }

        //api/appointments/2018-02-28
        [HttpGet]
        [Route("{day}")]
        public IEnumerable<Appointment> GetByDate(DateTime day)
        {
            return _service.GetByDate(day);
        }

        //api/appointments/2018-02-28/101
        [HttpGet]
        [Route("{day}/{id}")]
        public IEnumerable<Appointment> GetByAttendee(long id, DateTime day)
        {
            return _service.GetByAttendee(id, day);
        }

        [HttpPost]
        public Appointment Post([FromBody]CreateAppointmentRequestBase appointment)
        {
            _logger.LogInformation($"{appointment}");
            return _service.Create(appointment.ToModel());
        }

        [HttpPut]
        [Route("{id}/attendees")]
        public Appointment UpdateAttendeesList(long id, [FromBody] UpdateAttendeesListRequest request) {
            _logger.LogInformation($"update attendees request: {request} for id:{id}");
            return _service.UpdateAttendees(id, request.Attendees);
        }

        [HttpPut]
        [Route("{id}/date")]
        public Appointment UpdateDate(long id, [FromBody] UpdateDateRequest request) {
            _logger.LogInformation($"update date request: {request} for id:{id}");
            return _service.UpateDate(id, request.NewDate);   
        }
    }
}
