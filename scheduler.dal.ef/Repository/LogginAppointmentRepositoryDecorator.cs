
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using scheduler.interfaces.dal;
using scheduler.interfaces.dal.model;

namespace scheduler.dal.ef.Repository
{
    public class LogAppointmentRepositoryDecorator : IAppointmentRepository
    {
        private readonly IAppointmentRepository _decorated;
        private readonly ILogger _logger;
        public LogAppointmentRepositoryDecorator(IAppointmentRepository decorated, ILoggerFactory lf)
        {
            this._decorated = decorated;
            _logger = lf.CreateLogger<LogAppointmentRepositoryDecorator>();
        }

        public Appointment Create(Appointment newAppointment)
        {
            try
            {
                _logger.LogDebug($"Creating new appointment {newAppointment}");
                return _decorated.Create(newAppointment);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Failed to persiste appointment: {newAppointment}");
                throw;
            }
        }

        public IEnumerable<Appointment> GetByAttendee(long attendeeId, DateTime day)
        {
            try
            {
                _logger.LogDebug($"GetByAttendee attendee:{attendeeId} day:{day}");
                return _decorated.GetByAttendee(attendeeId, day);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Failed to get appointment for attendee:{attendeeId} day:{day}");
                throw;
            }
        }

        public IEnumerable<Appointment> GetByDate(DateTime day)
        {
            try
            {
                _logger.LogDebug($"GetByDate day:{day}");
                return _decorated.GetByDate(day);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Failed to get appointment for day:{day}");
                throw;
            }
        }

        public Appointment GetById(long id)
        {
            try
            {
                _logger.LogDebug($"GetById:{id}");
                return _decorated.GetById(id);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Failed getById:{id}");
                throw;
            }
        }

        public Appointment UpdateAttendeers(long meetingId, IList<Attendee> attenders)
        {
            try
            {
                _logger.LogDebug($"UpdateAttendeers meetingId:{meetingId} attenders:{attenders}");
                return _decorated.UpdateAttendeers(meetingId, attenders);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Failed to update UpdateAttendeers for meeting:{meetingId} attenders:{attenders}");

                throw;
            }
        }

        public Appointment UpdateDate(long reminderId, DateTime newDate)
        {
            try
            {
                _logger.LogDebug($"UpdateDate reminderId:{reminderId} newDate:{newDate}");
                return _decorated.UpdateDate(reminderId, newDate);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Failed to update reminder:{reminderId} with  newDate:{newDate}");

                throw;
            }
        }
    }
}