﻿using System;
using System.Collections.Generic;
using System.Linq;
using scheduler.interfaces.bll.model;

namespace scheduler.interfaces.dal.model
{
    public class OneOffMeeting : Appointment, IMeetingOccurrence
    {
        public OneOffMeeting(long id, DateTime start, DateTime end) : base(id, start)
        {
            Attenders = new List<AppointmentAttendee>();
            End = end;
        }


        public DateTime End { get; set; }
        public IList<AppointmentAttendee> Attenders { get; set; }

        public override DateTime GetMinDate => Start;

        public override DateTime GetMaxDate => End;

        public override bll.model.Appointment ToModel()
        {
            return new bll.model.OneOffMeeting(this.Id, 
                this.Attenders.Select(a => a.ToModel()).ToList(),
                new  bll.model.Occurrence(this.Start, this.End));
        }

        public override void UpdateDate(DateTime newDateTime)
        {
            var oldStart = this.Start;
            var meetingSpan = this.End - this.Start;
            this.Start = newDateTime;
            this.End = this.Start.Add(meetingSpan);
        }

        public virtual IEnumerable<IMeetingOccurrence> GetOccurrences() {
            return new List<IMeetingOccurrence> {this};
        }

        public override bool IsConflicting(IMeetingOccurrence other, IOccurrenceComparator comp)
        {
            return comp.IsConflict(this, other);
        }
    }
}
