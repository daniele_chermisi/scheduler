﻿using System;
namespace scheduler.interfaces.dal.model
{
    public class Repetition : IMeetingOccurrence
    {
        protected Repetition(long id, long meetingId, DateTime start, DateTime end) {
            Id = id;
            MeetingId = meetingId;
            Start = start;
            End = end;
        }
        public Repetition(long id, RecurrentMeeting meeting, long meetingId, DateTime start, DateTime end) : this (id, meetingId, start, end)
        {
            Meeting = meeting;
        }

        public long Id { get; protected set; }
        public RecurrentMeeting Meeting { get; protected set; }
        public long MeetingId { get; protected set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public bll.model.Occurrence ToModel() {
            return new bll.model.Occurrence(this.Start, this.End);
        }
    }
}
