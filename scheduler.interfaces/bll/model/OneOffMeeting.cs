﻿using System;
using System.Collections.Generic;
using System.Linq;
using scheduler.interfaces.dal.model;

namespace scheduler.interfaces.bll.model
{
	public class OneOffMeeting : AttendeedMeeting
    {
        public OneOffMeeting(long id, IList<Attendee> attenders, Occurrence occ) : base(id, attenders)
        {
            Occurrence = occ;
        }

        public Occurrence Occurrence { get; set; }

       
        public override dal.model.Appointment ToDal()
        {
            var res =  new scheduler.interfaces.dal.model.OneOffMeeting(
                this.Id,
                this.Occurrence.Start,
                this.Occurrence.End
            );
            AddAttenderFromModel(res);
            return res;
        }
    }
}
