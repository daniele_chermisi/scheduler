CREATE TABLE Repetition (
       Id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
       AppointmentId integer,
       Start Date NOT NULL,
        FOREIGN KEY(AppointmentId) REFERENCES Appointment(Id)
);

INSERT INTO Appointment (Discriminator, Start, End) VALUES ('RecurrentMeeting', '2018-02-01 08:30:00', '2018-02-01 09:30:00');
