﻿using System;
using scheduler.interfaces.dal;

namespace scheduler.interfaces.bll.model
{
    public abstract class Appointment
    {
        public Appointment(long id)
        {
            Id = id;
        }

        public long Id { get; set; }

        public abstract scheduler.interfaces.dal.model.Appointment ToDal();
    }
}
