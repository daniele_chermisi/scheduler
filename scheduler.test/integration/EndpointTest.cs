using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using scheduler.web;
using Xunit;

[Trait("Category", "EndToEnd")]
public class EndpointTest 
    : IClassFixture<WebApplicationFactory<Program>>
{
    private readonly WebApplicationFactory<Program> _factory;

    public EndpointTest(WebApplicationFactory<Program> factory)
    {
        _factory = factory;
    }

    // TODO: see this topic to deserialize response
    // https://stackoverflow.com/questions/12638741/deserialising-json-to-derived-types-in-asp-net-web-api
    [Theory]
    [InlineData("/api/appointments/2018-02-01")]
    [InlineData("/api/appointments/2018-02-01/1")]
    public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
    {
        // Arrange
        var client = _factory.CreateClient();

        // Act
        var response = await client.GetAsync(url);

        // Assert
        response.EnsureSuccessStatusCode(); // Status Code 200-299
        Assert.Equal("application/json; charset=utf-8", 
            response.Content.Headers.ContentType.ToString());

        var result  = response.Content.ReadAsStringAsync().Result;
        // var s = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(result);
        var sen = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<dynamic>>(result);    
        Assert.NotEmpty(sen);
    }
}