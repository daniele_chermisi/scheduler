﻿using System;
namespace scheduler.interfaces.bll.model
{
  public class Occurrence {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public Occurrence(DateTime s, DateTime e) {
            Start = s;
            End = e;
        }

        public scheduler.interfaces.dal.model.Repetition ToDal(dal.model.RecurrentMeeting  meeting) {
            return new scheduler.interfaces.dal.model.Repetition(0,
                meeting, meeting.Id, this.Start, this.End
            );
        }
    }
}
