﻿using System;
using Newtonsoft.Json;
using scheduler.web.dto;
using scheduler.interfaces.bll.model;
using scheduler.interfaces.bll;

namespace scheduler.web.dto.model
{
    public class UpdateDateRequest
    {
        public DateTime NewDate {get;set;}
       
    }  
}
