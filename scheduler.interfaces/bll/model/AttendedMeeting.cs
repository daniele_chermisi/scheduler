using System.Collections.Generic;
using System.Linq;

namespace scheduler.interfaces.bll.model
{
    public abstract class AttendeedMeeting : Appointment
    {
        public AttendeedMeeting(long id, IList<Attendee> attenders) : base(id)
        {
            Attenders = attenders;
        }

        public IList<Attendee> Attenders { get; set; }

        protected void AddAttenderFromModel(scheduler.interfaces.dal.model.OneOffMeeting res) {
            res.Attenders = this.Attenders
            .Select( model => new dal.model.AppointmentAttendee(0,
                res, res.Id, null, model.Id
            )).ToList();
        }
    }
}