SELECT 
    -- * 
    app.Id, otherapp.Id as OtherId, app.Start as Start , app.End as End, otherapp.Start as OtherStart,
        otherapp.End as OtherEnd, rep.Start as RepStart, otherrep.Start as OtherRepEnd,
        -- otherrep.Start + (julianday(otherapp.End) - julianday(otherapp.Start))
        DATETIME(otherrep.Start, '+300 minutes'),
        strftime('%s',otherapp.End) - strftime('%s',otherapp.Start)
    FROM Appointment app 
        INNER JOIN AppointmentToAttendee att on app.Id = att.AppointmentId
        LEFT JOIN Repetition rep on app.Id = rep.AppointmentId
    
    LEFT JOIN Appointment otherapp ON otherapp.Id != app.Id
        INNER JOIN AppointmentToAttendee otheratt on otherapp.Id = otheratt.AppointmentId
        LEFT JOIN Repetition otherrep on otherapp.Id = otherrep.AppointmentId
    WHERE app.Id = 3 
        AND
         ( 
             (otherapp.End > '2016-01-02' AND otherapp.Start < '2020-01-20')
             OR 
             (otherrep.Start > '2016-01-02')
         );