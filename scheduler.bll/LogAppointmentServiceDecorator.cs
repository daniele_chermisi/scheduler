
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using scheduler.interfaces.bll;
using scheduler.interfaces.bll.model;

namespace scheduler.bll
{
    public class LogAppointmentServiceDecorator : IAppointmentService
    {
        private readonly IAppointmentService _decorated;

        private readonly ILogger _logger;
        public LogAppointmentServiceDecorator(IAppointmentService decorated, ILoggerFactory lf)
        {
            _logger = lf.CreateLogger<LogAppointmentServiceDecorator>();
            _decorated = decorated;
        }

        public Appointment Create(Appointment appointment)
        {
            try
            {
                _logger.LogDebug($"Creating appointment: {appointment}");
                return _decorated.Create(appointment);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Creating appointment: {appointment}");
                throw;
            }
        }

        public IEnumerable<Appointment> GetByAttendee(long id, DateTime day)
        {
            try
            {
                _logger.LogDebug($"Getting by attendeeId:{id} day:{day}");
                return _decorated.GetByAttendee(id, day);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Getting appointment by attendeeId:{id} day:{day}");
                throw;
            }
        }

        public IEnumerable<Appointment> GetByDate(DateTime day)
        {
            try
            {
                _logger.LogDebug($"Getting by date: {day}");
                return _decorated.GetByDate(day);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Getting by date: {day}");
                throw;
            }
        }

        public Appointment UpateDate(long id, DateTime newDate)
        {
            try
            {
                _logger.LogDebug($"Updating Date for appointment id:{id} newDate:{newDate}");
                return _decorated.UpateDate(id, newDate);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Updating Date for appointment id:{id} newDate:{newDate}");
                throw;
            }
        }

        public Appointment UpdateAttendees(long id, IList<long> updatedAttendees)
        {
            try
            {
                _logger.LogDebug($"Updating attendees for appointment id:{id}");
                return _decorated.UpdateAttendees(id, updatedAttendees);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"Updating attendees for appointment id:{id}");
                throw;
            }
        }
    }
}