﻿using System;
using scheduler.interfaces.dal.model;

namespace scheduler.interfaces.bll.model
{
	public class Reminder : Appointment
    {
        public Reminder(long id, DateTime dt) : base(id)
        {
            Time = dt;
        }
        public DateTime Time { get; set; }

        public override dal.model.Appointment ToDal()
        {
            return new dal.model.Reminder(this.Id, this.Time);
        }
    }
}
