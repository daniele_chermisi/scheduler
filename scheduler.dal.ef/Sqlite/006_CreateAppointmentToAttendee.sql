CREATE TABLE Attendee (
        Id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
        FirstName text,
        LastName text
);
DROP TABLE AppointmentToAttendee;
CREATE TABLE AppointmentToAttendee (
        Id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
        AppointmentId integer,
        AttendeeId integer,
        FOREIGN KEY(AppointmentId) REFERENCES Appointment(Id),
        FOREIGN KEY(AttendeeId) REFERENCES Attendee(Id),
        UNIQUE(AppointmentId, AttendeeId)
);

  

INSERT INTO Attendee (FirstName, LastName) VALUES ('Mario', 'Rossi');
INSERT INTO Attendee (FirstName, LastName) VALUES ('Vincenzo', 'Verdi');
INSERT INTO Attendee (FirstName, LastName) VALUES ('Antonio', 'Bianchi');


INSERT INTO AppointmentToAttendee (AppointmentId, AttendeeId) VALUES (2, 1);
INSERT INTO AppointmentToAttendee (AppointmentId, AttendeeId) VALUES (3, 2);
INSERT INTO AppointmentToAttendee (AppointmentId, AttendeeId) VALUES (3, 3);