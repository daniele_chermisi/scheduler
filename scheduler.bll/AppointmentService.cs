﻿using System;
using System.Collections.Generic;
using System.Linq;
using scheduler.interfaces.bll;
using scheduler.interfaces.bll.model;
using scheduler.interfaces.dal;
using Microsoft.Extensions.Logging;

namespace scheduler.bll
{
    public class AppointmentService: IAppointmentService
    {
        private readonly IAppointmentRepository _repo;
        private readonly ILogger _logger;

        public AppointmentService(IAppointmentRepository repo, ILoggerFactory lf)
        {
            _repo = repo;
            _logger = lf.CreateLogger<AppointmentService>();
        }

        public IEnumerable<Appointment> GetByAttendee(long id, DateTime day)
        {
            return _repo.GetByAttendee(id, day).Select(m => m.ToModel());
        }

        public IEnumerable<Appointment> GetByDate(DateTime day)
        {
            return _repo.GetByDate(day).Select(m => m.ToModel());
        }

        public Appointment Create(Appointment appointment)
        {
            var res =  _repo.Create(appointment.ToDal());
            var resWithAttenders = _repo.GetById(res.Id);
            return  resWithAttenders.ToModel();
        }

        public Appointment UpdateAttendees(long id, IList<long> updatedAttendees)
        {
            var updated = _repo.UpdateAttendeers(id, 
                updatedAttendees
                    .Select(attId => new interfaces.dal.model.Attendee(attId, null, null)).ToList()
            );
            if (updated == null)
                return null;

            var resWithAttenders = _repo.GetById(updated.Id);
            return resWithAttenders.ToModel();   
        }

        public Appointment UpateDate(long id, DateTime newDate)
        {
            var updated = _repo.UpdateDate(id, newDate);
            if (updated == null)
                return null;
            return updated.ToModel();
        }
    }
}
