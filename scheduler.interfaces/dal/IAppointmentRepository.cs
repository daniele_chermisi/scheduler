﻿using System;
using System.Collections.Generic;
using scheduler.interfaces.dal.model;

namespace scheduler.interfaces.dal
{
    public interface IAppointmentRepository
    {
        Appointment GetById(long id);
        IEnumerable<Appointment> GetByDate(DateTime day);
        IEnumerable<Appointment> GetByAttendee(long attendeeId, DateTime day);
        Appointment UpdateAttendeers(long meetingId, IList<Attendee> attenders);
        Appointment UpdateDate(long reminderId, DateTime newDate);
        Appointment Create(Appointment newAppointment);
    }
}
