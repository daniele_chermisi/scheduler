using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using scheduler.interfaces.dal.model;

namespace scheduler.Data
{
    public class ApplicationDbContext : DbContext
    {
        public virtual DbSet<Appointment> Appointments { get; set; }
        public virtual DbSet<AppointmentAttendee> AppointmentAttendees { get; set; }
        public virtual DbSet<RecurrentMeeting> RecurrentMeetings { get; set; }
        public virtual DbSet<OneOffMeeting> OneOffMeetings { get; set; }
        public virtual DbSet<Reminder> Reminders { get; set; }
        public virtual DbSet<Attendee> Attenders { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<AppointmentAttendee>(entity =>
            {
                entity.ToTable("AppointmentToAttendee");
                entity.HasKey(e => e.Id);
                entity.HasOne(e => e.Appointment).WithMany(a => a.Attenders).HasForeignKey(e => e.AppointmentId);
                entity.HasOne(e => e.Attendee);
                
            });
            modelBuilder.Entity<Appointment>(entity =>
            {
                entity.ToTable("Appointment");
                entity.HasKey(e => e.Id);
                entity.HasDiscriminator<string>("Discriminator");
                entity.Property(e => e.Start).HasColumnType("Date").HasColumnName("Start");
            });

            modelBuilder.Entity<Reminder>(entity =>
            {
                entity.ToTable("Reminder");
                entity.HasBaseType<Appointment>();
                entity.Property(e => e.Start).HasColumnType("Date").HasColumnName("Start");
            });

            modelBuilder.Entity<OneOffMeeting>(entity =>
            {
                entity.ToTable("OneOffMeeting");
                entity.HasBaseType<Appointment>();
                entity.Property(e => e.Start).HasColumnType("Date").HasColumnName("Start");
                entity.Property(e => e.End).HasColumnType("Date").HasColumnName("End");
            });
            
            modelBuilder.Entity<RecurrentMeeting>(entity =>
            {
                entity.ToTable("OneOffMeeting");
                entity.HasBaseType<OneOffMeeting>();
                entity.Property(e => e.Start).HasColumnType("Date").HasColumnName("Start");
                entity.Property(e => e.End).HasColumnType("Date").HasColumnName("End");
                entity.HasMany(p => p.Repetitions).WithOne(a => a.Meeting).HasForeignKey(a => a.MeetingId);
            });

            modelBuilder.Entity<Repetition>(entity => {
                entity.ToTable("Repetition");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.MeetingId).HasColumnName("AppointmentId");
            });

            modelBuilder.Entity<Attendee>(entity => {
                entity.ToTable("Attendee");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.FirstName).HasColumnType("text");
                entity.Property(e => e.LastName).HasColumnType("text");
            });
        }
    }
}
