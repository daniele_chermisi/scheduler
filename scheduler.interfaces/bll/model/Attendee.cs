﻿using System;
namespace scheduler.interfaces.bll.model
{
    public class Attendee
    {
        public Attendee(long id)
        {
            Id = id;
        }
        public long Id {get; protected set;}
    }
}
