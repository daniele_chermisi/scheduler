﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace scheduler.test.unit
{
    [Trait("Category", "Unit")]
    public class CreateAppointmentRequestTest
    {
        public CreateAppointmentRequestTest()
        {
        }

        // TODO: split in more specific test cases
        [Fact]
        public void RecurrentMeeting_ToModel() {
            var request = new scheduler.web.dto.model.CreateRecurrentMeetingRequest(7) {
                Attenders = new List<long>() { 1 },
                Start = new DateTime(2020, 1, 1, 10, 30, 0),
                End = new DateTime(2020, 1, 1, 11, 0, 0),
                WeeklyRepetitions = 2
            };

            var model = request.ToModel();
            Assert.IsType<scheduler.interfaces.bll.model.RecurrentMeeting>(model);
            var castedModel = model as scheduler.interfaces.bll.model.RecurrentMeeting;
            var occurrences = castedModel.Occurrences;
            Assert.Equal(3, occurrences.Count);
            var first = occurrences.First();
            Assert.Equal(request.Start, first.Start);
            Assert.Equal(request.End, first.End);
            var second = occurrences[1];
            Assert.Equal(new DateTime(2020, 1, 8, 10, 30, 0), second.Start);
            Assert.Equal(new DateTime(2020, 1, 8, 11, 0, 0), second.End);
            var third = occurrences[2];
            Assert.Equal(new DateTime(2020, 1, 15, 10, 30, 0), third.Start);
            Assert.Equal(new DateTime(2020, 1, 15, 11, 0, 0), third.End);
        }
    }
}
