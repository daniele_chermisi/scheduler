
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using scheduler.dal.ef.Repository;
using scheduler.interfaces.dal.model;
using Xunit;

namespace scheduler.test.dal
{
    [Trait("Category", "Integration")]
    public class AppointmentRepositoryTest : NeverCommitIntegrationTest
    {
        private AppointmentRepository _sut;
        private readonly ObjectMother _mother;

        private readonly DateTime _newStart;
        private readonly DateTime _newEnd;

        public AppointmentRepositoryTest()
        {
            _sut = new AppointmentRepository(_db, new OccurrenceComparator(), _loggerFactory);
            _mother = new ObjectMother();
            _newStart = new System.DateTime(2015, 10, 1, 19, 0, 0);
            _newEnd = new System.DateTime(2015, 10, 1, 19, 30, 0);
        }

        [Fact]
        public void CreateReminder_ReminderIsSaved()
        {
            // ACT
            var saved = _sut.Create(_mother.Transient.MorningReminder);
            // ASSERT
            var read = _sut.GetByDate(_mother.Transient.MorningReminder.Start)
                .Single(r => r.Id == saved.Id);
            Assert.IsType<Reminder>(read);
            var readAsReminder = read as Reminder;
            Assert.Equal(_mother.Transient.MorningReminder.Start, readAsReminder.Start);
        }

        [Fact]
        public void CreateOneOffMeeting_MeetingIsSaved()
        {
            // ACT
            var saved = _sut.Create(_mother.Transient.MarioRossiOneOffMeeting);
            // ASSERT
            var read = _sut.GetByDate(_mother.Transient.MarioRossiOneOffMeeting.Start)
                .Single(r => r.Id == saved.Id);
            Assert.IsType<OneOffMeeting>(read);
            var readAsReminder = read as OneOffMeeting;
            Assert.Equal(_mother.Transient.MarioRossiOneOffMeeting.Start, readAsReminder.Start);
        }

        [Fact]
        public void CreateOneOffMeeting_AttendeeRelationIsSaved()
        {
            // ACT
            var saved = _sut.Create(_mother.Transient.MarioRossiOneOffMeeting);
            // ASSERT
            //TODO: write dedicated test case for GetByDate
            var read = _sut.GetByDate(_mother.Transient.MarioRossiOneOffMeeting.Start)
                .Single(r => r.Id == saved.Id);
            Assert.IsType<OneOffMeeting>(read);

            var readWithAttender = _db.OneOffMeetings
                .Include(oom => oom.Attenders)
                .ThenInclude(att => att.Attendee)
                .Single(oom => oom.Id == saved.Id);
            var readAsReminder = read as OneOffMeeting;
            Assert.Equal(_mother.Persisted.MarioRossi.Id, readAsReminder.Attenders.Select(a => a.Attendee).First().Id);
        }

        [Fact]
        public void Create_RepetitionMeeting_MeetingIsSaved()
        {
            // ACT
            var saved = _sut.Create(_mother.Transient.AntonioBianchiRecurringMeeting);
            // ASSERT
            var read = _sut.GetByDate(_mother.Transient.AntonioBianchiRecurringMeeting.Start)
                .Single(r => r.Id == saved.Id);
            Assert.IsType<RecurrentMeeting>(read);
            var readAsReminder = read as RecurrentMeeting;
            Assert.Equal(_mother.Transient.AntonioBianchiRecurringMeeting.Start, readAsReminder.Start);
        }

        [Fact]
        public void UpdateDate_Reminder_DateIsUpdated()
        {

            _sut.UpdateDate(_mother.Persisted.Reminder.Id, _newStart);
            // ASSERT
            var updated = _db.Reminders.SingleOrDefault(r => r.Id == _mother.Persisted.Reminder.Id);
            Assert.Equal(_newStart, updated.Start);
        }

        [Fact]
        public void UpdateDate_RecurrentMeeting_DatesAreUpdated()
        {
            // ACT
            var newStart = new DateTime(2018, 02, 03, 09, 30, 00);
            _sut.UpdateDate(_mother.Persisted.RecurrentMeeting.Id, new DateTime(2018, 02, 03, 09, 30, 00));
            // ASSERT
            // new DateTime(2018, 02, 01, 08, 30, 00), new DateTime(2018, 02, 01, 09, 30, 00)
            var updated = _db.OneOffMeetings.SingleOrDefault(m => m.Id == _mother.Persisted.RecurrentMeeting.Id);
            Assert.Equal(newStart, updated.Start);
            Assert.Equal(new DateTime(2018, 02, 03, 10, 30, 00), updated.End);
        }

        [Fact]
        public void UpdateDate_RecurrentMeeting_OccurrenceesAreUpdated()
        {
            // ACT
            var newStart = new DateTime(2018, 02, 03, 09, 30, 00);
            _sut.UpdateDate(_mother.Persisted.RecurrentMeeting.Id, new DateTime(2018, 02, 03, 09, 30, 00));
            // ASSERT
            // new DateTime(2018, 02, 01, 08, 30, 00), new DateTime(2018, 02, 01, 09, 30, 00)
            var updated = _db.RecurrentMeetings
                .Include(m => m.Repetitions)
                .SingleOrDefault(m => m.Id == _mother.Persisted.RecurrentMeeting.Id);
            var firstRep = updated.Repetitions[0];
            Assert.Equal(new DateTime(2018, 02, 10, 09, 30, 00), firstRep.Start);
        }

        [Fact]
        public void UpdateAttendees_RecurrentMeeting_UpdateAttendees()
        {
            // ACT
            // _sut.UpdateAttendeers(_mother.Persisted.RecurrentMeeting.Id, new List<Attendee>() { _mother.Persisted.MarioRossi });
            _sut.UpdateAttendeers(_mother.Persisted.RecurrentMeeting.Id, new List<Attendee>() {
                new Attendee(_mother.Persisted.MarioRossi.Id, null, null)
            });
            // ASSERT
            var updated = _db.RecurrentMeetings
                .Include(m => m.Attenders)
                .ThenInclude(ma => ma.Attendee)
                .SingleOrDefault(m => m.Id == _mother.Persisted.RecurrentMeeting.Id);

            Assert.Equal(1, updated.Attenders.Count);
            Assert.Equal(_mother.Persisted.MarioRossi.Id, updated.Attenders.First().Attendee.Id);
            Assert.Equal(_mother.Persisted.MarioRossi.FirstName, updated.Attenders.First().Attendee.FirstName);
            Assert.Equal(_mother.Persisted.MarioRossi.LastName, updated.Attenders.First().Attendee.LastName);
        }
        [Fact]
        public void UpdateAttendees_RecurrentMeeting_NotExistingAttendee_Throws()
        {
            // ACT
            Assert.Throws<Microsoft.EntityFrameworkCore.DbUpdateException>(
                () =>
                    {
                        _sut.UpdateAttendeers(_mother.Persisted.RecurrentMeeting.Id, new List<Attendee>() { new Attendee(1500, "A", "B") });
                    }
            );
        }

        [Fact]
        public void GetAppointments_NoOffendingAppointment_ReturnNoConflict()
        {
            // ARRANMGE
            var saved  = _mother.Transient.NoOffendingRecurringMeeting_DifferentDates_SameAttendees;
            _db.Appointments.Add(saved);
            Assert.True(_db.SaveChanges() > 0);
            // ACT
            var recMeeting = _sut.GetConflicting(saved.Id).ToList();

            // ASSERT
            Assert.Empty(recMeeting);
        }

        [Fact]
        public void GetAppointments_OffendingAppointment_ReturnConflict()
        {
              // ARRANMGE
            var saved  = _mother.Transient.OffendingRecurringMeeting_SameRepetition;
            _db.Appointments.Add(saved);
            Assert.True(_db.SaveChanges() > 0);
            // ACT
            var recMeeting = _sut.GetConflicting(saved.Id).ToList();

            // ASSERT
            Assert.NotEmpty(recMeeting);
        }

        [Fact]
        public void GetAppointments_OffendingAppointment1_ReturnConflict()
        {
              // ARRANMGE
            var saved  = _mother.Transient.OffendingRecurringMeeting_RepetitionEqualDate;
            _db.Appointments.Add(saved);
            Assert.True(_db.SaveChanges() > 0);
            // ACT
            var recMeeting = _sut.GetConflicting(saved.Id).ToList();

            // ASSERT
            Assert.NotEmpty(recMeeting);
        }

        [Fact]
        public void GetAppointments_OffendingAppointment2_ReturnConflict()
        {
              // ARRANMGE
            var saved  = _mother.Transient.OffendingRecurringMeeting_SameDates;
            _db.Appointments.Add(saved);
            Assert.True(_db.SaveChanges() > 0);
            // ACT
            var recMeeting = _sut.GetConflicting(saved.Id).ToList();

            // ASSERT
            Assert.NotEmpty(recMeeting);
        }
    }
}