using scheduler.interfaces.dal;
namespace scheduler.dal.ef.Repository
{
    public class OccurrenceComparator : IOccurrenceComparator
    {
        public bool IsConflict(IMeetingOccurrence a, IMeetingOccurrence b) {
            if (a.Start <= b.End && a.End >= b.Start) return true;
            return false;
        }
    }
}