using System;
using System.Collections.Generic;
using System.Linq;

namespace scheduler.interfaces.dal.model
{
    public class RecurrentMeeting : OneOffMeeting, IMeetingOccurrence
    {
        public RecurrentMeeting(long id, DateTime start, DateTime end) : base(id, start, end) {
            Repetitions = new List<Repetition>();
        }

        public IList<Repetition> Repetitions { get; set; }

        public override bll.model.Appointment ToModel()
        {
            var firstOccurrence = new  bll.model.Occurrence(this.Start, this.End);
            var allOccurrence = new List<bll.model.Occurrence>() { firstOccurrence };
            allOccurrence.AddRange(Repetitions.Select(r => r.ToModel()).ToList());
            var res =  new bll.model.RecurrentMeeting(this.Id, 
                this.Attenders.Select(a => a.ToModel()).ToList(),
                allOccurrence
            );
            
            return res;
        }
        public override void UpdateDate(DateTime newDateTime)
        {
            var changeSpan = newDateTime - this.Start;
            base.UpdateDate(newDateTime);
            
            foreach (var item in Repetitions)
            {
                item.Start = item.Start.Add(changeSpan);
            }
        }

        public override DateTime GetMaxDate {
            get {
                if (Repetitions == null || !Repetitions.Any())
                {
                    throw new InvalidOperationException($"Recurrent meeting shoudl have repetitions. MeetingId:{this.Id}");
                }
                return Repetitions.Max(r => r.End);
            }
        }

        public override IEnumerable<IMeetingOccurrence> GetOccurrences() {
            var baseOcc =  base.GetOccurrences().ToList();
            baseOcc.AddRange(Repetitions.ToList());
            return baseOcc;
        }

        public override bool IsConflicting(IMeetingOccurrence other, IOccurrenceComparator comp) {
            var baseResult = base.IsConflicting(other, comp);
            if (baseResult) return true;
                
            foreach (var occ in Repetitions)
            {
                if (comp.IsConflict(occ, other)) return true;
            }

            return false;
        }
    }
}