﻿using System;
namespace scheduler.interfaces.dal.model
{
    public class Attendee
    {
        public Attendee(long id, string firstName, string lastName)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
        }
        public long Id {get; protected set;}
        public string FirstName {get;protected set;}
        public string LastName {get;protected set;}
    }
}
