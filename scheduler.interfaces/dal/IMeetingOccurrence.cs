using System;
using System.Collections.Generic;
using scheduler.interfaces.dal.model;

namespace scheduler.interfaces.dal
{
    public interface IMeetingOccurrence
    {
        DateTime Start {get;}
        DateTime End {get;}
    }
}