﻿using System;
using System.Collections.Generic;
using scheduler.interfaces.dal.model;

namespace scheduler.test.dal
{
    public class ObjectMother
    {
        public class TransientMother
        {
            private PersistedMother _persisted;
            public TransientMother(PersistedMother p)
            {
                _persisted = p;
            }

            public Repetition NewYearDay(RecurrentMeeting meeting) 
            {
                
                {
                    return new Repetition(0, meeting, meeting.Id, new DateTime(2019, 1, 1, 0, 0, 0), new DateTime(2019, 1, 1, 0, 30, 0) );
                }
            }
            public Reminder MorningReminder
            {
                get
                {
                    return new Reminder(0, new DateTime(2018, 03, 29, 7, 0, 0));
                }
            }

            // TODO: please notice the null reference to meeting
            public OneOffMeeting MarioRossiOneOffMeeting
            {
                get
                {
                    var res = new OneOffMeeting(0, new DateTime(2018, 04, 30, 10, 0, 0), new DateTime(2018, 04, 30, 10, 30, 0));
                    res.Attenders = new List<AppointmentAttendee>() { 
                        new AppointmentAttendee(0, res, res.Id, null, _persisted.MarioRossi.Id)
                    };
                    return res;
                }
            }

            public RecurrentMeeting AntonioBianchiRecurringMeeting
            {
                get
                {
                    var res = new RecurrentMeeting(0, new DateTime(2018, 04, 30, 10, 0, 0), new DateTime(2018, 04, 30, 10, 30, 0));
                    // TODO: please note the null reference to AntonioBianchi.
                    // Passing reference to the AppointmentAttendee EF does ain insert of AntonioBianchi
                    res.Attenders = new List<AppointmentAttendee>() { 
                        new AppointmentAttendee(0, null, 0, null, _persisted.AntonioBianchi.Id)
                    };
                    res.Repetitions.Add( this.NewYearDay(res));
                    return res;
                }
            }
        

            public RecurrentMeeting OffendingRecurringMeeting_SameDates
            {
                get 
                {
                    var existingMeetiing = _persisted.RecurrentMeeting;
                    var existingRepetition = existingMeetiing.Repetitions[0];
                    var res = new RecurrentMeeting(0, existingMeetiing.Start, existingMeetiing.End);
                    res.Repetitions =new List<Repetition>() { 
                        new Repetition(0, null, 0, existingRepetition.Start.AddDays(1),
                            existingRepetition.End.AddDays(1) )
                     };
                    res.Attenders =new List<AppointmentAttendee>() { 
                        new AppointmentAttendee(0, null, 0, null, this._persisted.VincenzoVersi.Id)
                    };
                    return res;
                }
            }

            public RecurrentMeeting OffendingRecurringMeeting_SameRepetition
            {
                get 
                {
                    var existingMeetiing = _persisted.RecurrentMeeting;
                    var existingRepetition = existingMeetiing.Repetitions[0];
                    var res = new RecurrentMeeting(0, existingMeetiing.Start.AddDays(-7), existingMeetiing.End.AddDays(-7));
                    res.Repetitions =new List<Repetition>() { 
                        new Repetition(0, null, 0, existingRepetition.Start,
                            existingRepetition.End )
                     };
                    res.Attenders =new List<AppointmentAttendee>() { 
                        new AppointmentAttendee(0, null, 0, null, this._persisted.VincenzoVersi.Id)
                    };
                    return res;
                }
            }
          
          
            public RecurrentMeeting OffendingRecurringMeeting_RepetitionEqualDate
            {
                get 
                {
                    var existingMeetiing = _persisted.RecurrentMeeting;
                    var existingRepetition = existingMeetiing.Repetitions[0];
                    var res = new RecurrentMeeting(0, existingMeetiing.Start.AddDays(-7), existingMeetiing.End.AddDays(-7));
                    res.Repetitions =new List<Repetition>() { 
                        new Repetition(0, null, 0, existingRepetition.Start.AddDays(-7),
                            existingRepetition.End.AddDays(-7) )
                     };
                    res.Attenders =new List<AppointmentAttendee>() { 
                        new AppointmentAttendee(0, null, 0, null, this._persisted.VincenzoVersi.Id)
                    };
                    return res;
                }
            }
          
          
            public RecurrentMeeting NoOffendingRecurringMeeting_SameDates_DifferentAttendees
            {
                get 
                {
                    var existingMeetiing = _persisted.RecurrentMeeting;
                    var existingRepetition = existingMeetiing.Repetitions[0];
                    var res = new RecurrentMeeting(0, existingMeetiing.Start, existingMeetiing.End);
                    res.Repetitions =new List<Repetition>() { 
                        new Repetition(0, null, 0, existingRepetition.Start, existingRepetition.End )
                     };
                    res.Attenders =new List<AppointmentAttendee>() { 
                        new AppointmentAttendee(0, null, 0, null, this._persisted.MarioRossi.Id)
                    };
                    return res;
                }
            }

            public RecurrentMeeting NoOffendingRecurringMeeting_DifferentDates_SameAttendees
            {
                get 
                {
                    var mshift = TimeSpan.FromDays(1);
                    var existingMeetiing = _persisted.RecurrentMeeting;
                    var existingRepetition = existingMeetiing.Repetitions[0];
                    var res = new RecurrentMeeting(0, existingMeetiing.Start.Add(mshift), existingMeetiing.End.Add(mshift));
                    res.Repetitions =new List<Repetition>() { 
                        new Repetition(0, null, 0, existingRepetition.Start.Add(mshift), existingRepetition.End.Add(mshift) )
                     };
                    res.Attenders =new List<AppointmentAttendee>() { 
                        new AppointmentAttendee(0, null, 0, null, this._persisted.AntonioBianchi.Id)
                    };
                    return res;
                }
            }
        }

        public class PersistedMother
        {
            public Reminder Reminder
            {
                get
                {
                    return new Reminder(1, new DateTime(2018, 02, 01, 08, 30, 0));
                }
            }

            public OneOffMeeting OneOffMeeting
            {
                get
                {
                    var res = new OneOffMeeting(2, new DateTime(2018, 02, 01, 08, 30, 00), new DateTime(2018, 02, 01, 09, 30, 00));
                    res.Attenders = new List<AppointmentAttendee>() { 
                        new AppointmentAttendee(1, null, 2, this.MarioRossi, this.MarioRossi.Id)
                    };
                    return res;
                }
            }

            public RecurrentMeeting RecurrentMeeting
            {
                get
                {
                    var res = new RecurrentMeeting(3, new DateTime(2018, 02, 01, 08, 30, 00), new DateTime(2018, 02, 01, 09, 30, 00));
                    res.Repetitions =new List<Repetition>() { MeetingRepetition(res) };
                    res.Attenders =new List<AppointmentAttendee>() { 
                        new AppointmentAttendee(2, null, 3, this.VincenzoVersi, this.VincenzoVersi.Id),
                        new AppointmentAttendee(3, null, 3, this.AntonioBianchi, this.AntonioBianchi.Id)
                    };
                    return res;
                }
            }

            public Attendee MarioRossi
            {
                get
                {
                    return new Attendee(1, "Mario", "Rossi");
                }
            }
            public Attendee VincenzoVersi
            {
                get
                {
                    return new Attendee(1, "Vincenzo", "Verdi");
                }
            }
            public Attendee AntonioBianchi
            {
                get
                {
                    return new Attendee(1, "Antonio", "Bianchi");
                }
            }

            public Repetition MeetingRepetition(RecurrentMeeting m)
            {
                {
                    return new Repetition(2, m, m.Id, new DateTime(2018, 02, 08, 08, 30, 00), new DateTime(2018, 02, 08, 9, 30, 00));
                }
            }
        }
        public ObjectMother()
        {
            Persisted = new PersistedMother();
            Transient = new TransientMother(Persisted);
        }
        public TransientMother Transient { get; private set; }
        public PersistedMother Persisted { get; private set; }
    }
}
