﻿using System;
using System.Collections.Generic;
using scheduler.interfaces.bll.model;

namespace scheduler.interfaces.bll
{
    public interface IAppointmentService
    {
        IEnumerable<Appointment> GetByDate(DateTime day);
        IEnumerable<Appointment> GetByAttendee(long id, DateTime day);
        Appointment Create(Appointment appointment);
        Appointment UpdateAttendees(long id, IList<long> updatedAttendees);
        Appointment UpateDate(long id, DateTime newDate);
    }
}
