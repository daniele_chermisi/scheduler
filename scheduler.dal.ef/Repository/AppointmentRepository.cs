﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using scheduler.Data;
using scheduler.interfaces.dal;
using scheduler.interfaces.dal.model;

namespace scheduler.dal.ef.Repository
{
    public class AppointmentRepository : IAppointmentRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly IOccurrenceComparator _comparator;
        private readonly ILogger _logger;
        public AppointmentRepository(ApplicationDbContext db, IOccurrenceComparator comparator, ILoggerFactory lf)
        {
            _db = db;
            this._comparator = comparator;
            _logger = lf.CreateLogger<AppointmentRepository>();
        }

        public Appointment GetById(long id)
        {
            return _db.Appointments
                .Include(m => (m as OneOffMeeting).Attenders)
                    .ThenInclude(ma => (ma as AppointmentAttendee).Attendee)
                .Include(m => (m as RecurrentMeeting).Repetitions)
                .SingleOrDefault(a => a.Id == id);
        }

        public IEnumerable<Appointment> GetByAttendee(long attendeeId, DateTime day)
        {
            var datePart = day.Date;
            return _db.OneOffMeetings
                .Include(m => (m as OneOffMeeting).Attenders)
                    .ThenInclude(ma => (ma as AppointmentAttendee).Attendee)
                .Include(m => (m as RecurrentMeeting).Repetitions)
                .Where(a => a.Start > datePart && a.Start < datePart.AddDays(1)
                    && a.Attenders.Select(at => at.AttendeeId).Contains(attendeeId));
        }

        public IEnumerable<Appointment> GetByDate(DateTime day)
        {
            var datePart = day.Date;
            return _db.Appointments
                .Include("Attenders")
                .Include("Attenders.Attendee")
                .Include("Repetitions")
                .Where(a => a.Start > datePart && a.Start < datePart.AddDays(1));
        }

        public IEnumerable<Appointment> GetConflicting(long id) 
        {
            var @checked = _db.OneOffMeetings
                .Include(m => m.Attenders)
                    .ThenInclude(ma => ma.Attendee)
                .Include(m => (m as RecurrentMeeting).Repetitions)
                .SingleOrDefault(a => a.Id == id);

            var minDate = @checked.GetMinDate;
            var maxDate = @checked.GetMaxDate;

            var res = _db.OneOffMeetings
                .Include(m => (m).Attenders)
                    .ThenInclude(ma => (ma).Attendee)
                .Include(m => (m as RecurrentMeeting).Repetitions)
                .Where(
                    other => (
                        other.Id != id
                        &&
                        (other.Start <= maxDate && other.End >= minDate)
                        ||
                        (
                            (other as RecurrentMeeting) != null 
                            &&
                            (other as RecurrentMeeting).Repetitions.Any()
                            &&
                            (other as RecurrentMeeting).Repetitions.Min(r => r.Start) >= maxDate
                            &&
                            (other as RecurrentMeeting).Repetitions.Max(r => r.End) >= minDate
                        )
                    )
                ).ToList();
            
            var checkedOccs = @checked.GetOccurrences();
            var conflicts = new List<OneOffMeeting>();
            foreach (var checkedOcc in checkedOccs)
            {
                foreach (var other in res)
                {
                    var otherOccs = other.GetOccurrences();
                    foreach (var otherOcc in otherOccs)
                    {
                        if (other.IsConflicting(checkedOcc, _comparator))
                        {
                            conflicts.Add(other);
                        }
                    }
                }
            }
            return conflicts;
        }


        public Appointment Create(Appointment newAppointment)
        {
            _db.Appointments.Add(newAppointment);

            if (_db.SaveChanges() > 0)
            {
                return newAppointment;
            }
            return null;
        }

        // TODO: improove performance avoid deleting all existing attenders
        public Appointment UpdateAttendeers(long meetingId, IList<Attendee> attenders)
        {
            if (attenders == null && !attenders.Any()) 
                return null;

            var result = _db.OneOffMeetings
                .Include(m => m.Attenders)
                .SingleOrDefault(m => m.Id == meetingId);
                
            if (result != null) {
                // Delete all
                foreach (var existing in result.Attenders)
                {
                    _db.AppointmentAttendees.Remove(existing);
                }
                foreach (var attender in attenders)
                {
                    result.Attenders.Add(new AppointmentAttendee(0, null, result.Id, null, attender.Id));
                }
                if (_db.SaveChanges() > 0) {
                    return result;
                }
                return null;
            }
            return null;
        }

        public Appointment UpdateDate(long reminderId, DateTime newDate)
        {
            var result = _db.Appointments
                .Include(m => (m as OneOffMeeting).Attenders)
                    .ThenInclude(ma => (ma as AppointmentAttendee).Attendee)
                .Include(m => (m as RecurrentMeeting).Repetitions)
                .SingleOrDefault(b => b.Id == reminderId);
            if (result != null)
            {
                result.UpdateDate(newDate);
                if (_db.SaveChanges() > 0 ) {
                    return result;
                }
            }
            return null;
        }
    }
}
