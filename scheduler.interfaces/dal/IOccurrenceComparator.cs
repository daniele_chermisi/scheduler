using System;
using System.Collections.Generic;
using scheduler.interfaces.dal.model;

namespace scheduler.interfaces.dal
{
    public interface IOccurrenceComparator
    {
        bool IsConflict(IMeetingOccurrence a, IMeetingOccurrence b);
    }
}