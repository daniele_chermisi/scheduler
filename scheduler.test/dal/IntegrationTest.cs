﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using scheduler.Data;

namespace scheduler.test.dal
{
    public class NeverCommitIntegrationTest : TestAbstract, IDisposable
    {
        protected ApplicationDbContext _db;
        private readonly IDbContextTransaction _trans;
    
        public NeverCommitIntegrationTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlite("DataSource=app.db");

            _db = new ApplicationDbContext(optionsBuilder.Options);

            _trans = _db.Database.BeginTransaction();
        }

        public void Dispose()
        {
            _trans.Dispose();
            _db.Dispose();
        }
    }
}
