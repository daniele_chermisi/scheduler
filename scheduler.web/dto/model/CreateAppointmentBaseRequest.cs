using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using scheduler.interfaces.bll.model;
using scheduler.web.dto;

namespace scheduler.web.dto.model 
{
    [JsonConverter(typeof(MyCustomConverter))]
    public abstract class CreateAppointmentRequestBase
    {
        // TODO: inject from configuration?
        public static int RepetitionSpanInDays = 7;
        private class MyCustomConverter : JsonCreationConverter<CreateAppointmentRequestBase>
        {
            protected override CreateAppointmentRequestBase Create(Type objectType,
              Newtonsoft.Json.Linq.JObject jObject)
            {
                //TODO: read the raw JSON object through jObject to identify the type
                //e.g. here I'm reading a 'typename' property:

                var typename = jObject.Value<string>("typename");
                if ("Reminder".Equals(typename))
                {
                    return new CreateReminderRequest();
                } else if ("OneOffMeeting".Equals(typename)) {
                    return new CreateOneOffMeetingRequest();
                } else if ("RecurrentMeeting".Equals(typename)) {
                    return new CreateRecurrentMeetingRequest(7);
                }
                return new UnsupportedRequest(typename);
                //now the base class' code will populate the returned object.
            }
        }

        public abstract Appointment ToModel();
    }

    public class UnsupportedRequest : CreateAppointmentRequestBase
    {
        private readonly string _requestedType;

        public UnsupportedRequest(string requestedType) {
            this._requestedType = requestedType;
        }
        public override Appointment ToModel()
        {
            throw new InvalidOperationException($"Create request not supported for typename value {_requestedType}");   
        }
    }
    public class CreateReminderRequest : CreateAppointmentRequestBase
    {
        public DateTime Start { get; set; }

        public override Appointment ToModel()
        {
            return new Reminder(0, this.Start);
        }

        public override string ToString() {
            return $"Start: {Start}";
        }
    }

    public abstract class CreateAttendedMeetingRequest: CreateAppointmentRequestBase { 
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public IList<long> Attenders { get; set; }

        public override string ToString() {
            return $"Start:{Start} End:{End} Attenders:{Attenders}";
        }

    }
    public class CreateOneOffMeetingRequest : CreateAttendedMeetingRequest
    {
        public override Appointment ToModel()
        {
            return new OneOffMeeting(0, 
                this.Attenders.Select(a => new Attendee(a)).ToList(),
                new Occurrence(this.Start, this.End));
        }

        public override string ToString() {
            return $"{base.ToString()}";
        }
    }
    public class CreateRecurrentMeetingRequest : CreateAttendedMeetingRequest
    {
        private readonly int _repetitionSpanInDays;

        public CreateRecurrentMeetingRequest(int repetitionSpanInDays) {
            _repetitionSpanInDays = repetitionSpanInDays;
        }
        public int WeeklyRepetitions { get; set; }

        public override Appointment ToModel()
        {
            var firstOcc = new Occurrence(this.Start, this.End);
            var meetingSpan = this.End - this.Start;
            var occurrences = new List<Occurrence>() { 
                firstOcc
            };
            for (int i = 0; i < this.WeeklyRepetitions; i++) {
                var curDaysSpan = (i + 1) * _repetitionSpanInDays;
                occurrences.Add(new Occurrence(this.Start.AddDays(curDaysSpan), this.End.AddDays(curDaysSpan)));
            }
            return new RecurrentMeeting(0, 
                this.Attenders.Select(a => new Attendee(a)).ToList(),
                occurrences
            );
        }

        public override string ToString() {
            return $"{base.ToString()} WeeklyRepetitions:{WeeklyRepetitions}";
        }
    }
}