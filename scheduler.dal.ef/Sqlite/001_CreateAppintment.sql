-- Reminder
-- CREATE TABLE Reminder (
--        Id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
 --       Time Date
--);

-- CREATE TABLE OneOffMeeting (
 --       Id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
 --       Start Date,
 --       End Date
--);


-- DROP TABLE OneOffMeeting;
-- DROP TABLE Reminder;
 CREATE TABLE Appointment (
       Id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
       Discriminator text NOT NULL,
       Start Date NOT NULL,
       End Date
);

