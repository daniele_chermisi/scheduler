﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using scheduler.Data;
using scheduler.interfaces.dal;
using scheduler.dal.ef.Repository;
using scheduler.interfaces.bll;
using scheduler.bll;
using Microsoft.AspNetCore.Mvc;
using scheduler.web.Filter;

namespace scheduler.web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddMvc();

            services.AddTransient<AppointmentRepository, AppointmentRepository>();
            services.AddTransient<AppointmentService, AppointmentService>();
            services.AddSingleton<IOccurrenceComparator, OccurrenceComparator>();

            services.AddTransient<IAppointmentRepository, LogAppointmentRepositoryDecorator>(sp =>
            {
                return new LogAppointmentRepositoryDecorator(
                    sp.GetService<AppointmentRepository>(),
                    sp.GetService<ILoggerFactory>()
                );
            });

            services.AddTransient<IAppointmentService, LogAppointmentServiceDecorator>(sp =>
            {
                return new LogAppointmentServiceDecorator(
                    sp.GetService<AppointmentService>(),
                    sp.GetService<ILoggerFactory>()
                );
            });

            services.Configure<MvcOptions>(options =>
            {
                // TODO: filters for custom errore response not yet implemented
                options.Filters.Add(new CustomExceptionFilter());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Localizzazione
            RequestLocalizationOptions localizationOptions = new RequestLocalizationOptions
            {
                SupportedCultures = new List<CultureInfo> { new CultureInfo("it-IT") },
                SupportedUICultures = new List<CultureInfo> { new CultureInfo("it-IT") }
            };
            app.UseRequestLocalization(localizationOptions);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // TODO: log configuration...

            app.UseMvc();
        }
    }
}
