﻿using System;
using System.Collections.Generic;
using System.Linq;
using scheduler.interfaces.dal.model;

namespace scheduler.interfaces.bll.model
{
    public class RecurrentMeeting : AttendeedMeeting
    {
        public RecurrentMeeting(long id, IList<Attendee> attenders, IList<Occurrence> occurrences) : base(id, attenders)
        {
            Occurrences = occurrences;
        }

        public IList<Occurrence> Occurrences { get; set; }

        public override dal.model.Appointment ToDal()
        {
            var firstOccurrence = this.Occurrences.First();
            var copiedOccurrencies = new List<Occurrence>(this.Occurrences);
            copiedOccurrencies.Remove(firstOccurrence);
            var res =  new dal.model.RecurrentMeeting(
                this.Id, firstOccurrence.Start, firstOccurrence.End
            );
            AddAttenderFromModel(res);
            res.Repetitions = copiedOccurrencies.Select(o => o.ToDal(res)).ToList();
            return res;
        }
    }
}
